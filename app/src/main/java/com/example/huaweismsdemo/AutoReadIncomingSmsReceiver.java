package com.example.huaweismsdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.huawei.hms.common.api.CommonStatusCodes;
import com.huawei.hms.support.api.client.Status;
import com.huawei.hms.support.sms.common.ReadSmsConstant;


public class AutoReadIncomingSmsReceiver extends BroadcastReceiver {
    public AutoReadSMSListener listener;
//    public static final String SMS_RETRIEVED_ACTION = "com.huawei.hms.auth.api.phone.SMS_RETRIEVED";

    public AutoReadIncomingSmsReceiver(AutoReadSMSListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (ReadSmsConstant.READ_SMS_BROADCAST_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                Status status = (Status) extras.get(ReadSmsConstant.EXTRA_STATUS);
                if (status != null) {
                    switch (status.getStatusCode()) {
                        case CommonStatusCodes.SUCCESS:
                            String message = (String) extras.get(ReadSmsConstant.EXTRA_SMS_MESSAGE);
                            Toast.makeText(context, "message: " + message, Toast.LENGTH_SHORT).show();
                            if (listener != null) {
                                listener.onReadSMS(message);
                            }
                            break;
                        case CommonStatusCodes.TIMEOUT:
                            if (listener != null) {
                                listener.onTimeOutReadSms(null);
                            }
                            break;
                    }
                }
            }
        }
    }

    public interface AutoReadSMSListener {
        void onReadSMS(String otp);

        void onTimeOutReadSms(String otp);
    }
}