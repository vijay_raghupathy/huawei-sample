package com.example.huaweismsdemo;

import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.huawei.hmf.tasks.OnCompleteListener;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hmf.tasks.Task;
import com.huawei.hms.common.ApiException;
import com.huawei.hms.support.sms.ReadSmsManager;
import com.huawei.hms.support.sms.common.ReadSmsConstant;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements AutoReadIncomingSmsReceiver.AutoReadSMSListener {


    private AutoReadIncomingSmsReceiver autoReadIncomingSmsReceiver;
    TextView text_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text_view = findViewById(R.id.text_view);
//        new AppSignatureHelper(this).getAppSignatures();// UomdTO8UJRh
        Log.e("Hash Value -- ", new HuaweiHashGenerator(this).getAppSignature());

        AutoReadIncomingSmsReceiver autoReadIncomingSmsReceiver = new AutoReadIncomingSmsReceiver(new AutoReadIncomingSmsReceiver.AutoReadSMSListener() {
            @Override
            public void onReadSMS(String content) {
                // Read the message within rules successfully
//                resultTextView.setText(content);
                Toast.makeText(MainActivity.this, "onReceived: "+content, Toast.LENGTH_SHORT).show();
                Log.i("onReceived", "SmsRetrieverReceiver, content is: " + content);
            }

            @Override
            public void onTimeOutReadSms(String content) {
                // Read the message overtime
//                resultTextView.setText("onTimeOut");
                Log.e("onTimeOut", "SmsRetrieverReceiver, timeout");
            }
        });
        IntentFilter filter = new IntentFilter();
        filter.addAction(ReadSmsConstant.READ_SMS_BROADCAST_ACTION);
        registerReceiver(autoReadIncomingSmsReceiver, filter);


        /*autoReadIncomingSmsReceiver = new AutoReadIncomingSmsReceiver();
        autoReadIncomingSmsReceiver.setListener(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ReadSmsConstant.READ_SMS_BROADCAST_ACTION);
        getApplicationContext().registerReceiver(autoReadIncomingSmsReceiver, intentFilter);
        ReadSmsManager.start(this);*/
        text_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSmsRetriever();
            }
        });
    }

    private void startSmsRetriever() {

        Task<Void> task = ReadSmsManager.start(MainActivity.this);

        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                text_view.setText("startSmsRetriever, Complete: ");
                Log.i("isComplete", "Complete: ");
                Toast.makeText(MainActivity.this, "onSuccess: ", Toast.LENGTH_SHORT).show();
            }
        });

        task.addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(Task<Void> task) {
//                if (task.isSuccessful()) {
                    text_view.setText("startSmsRetriever, onSuccess: ");
                    Log.i("isSuccessful", "onSuccess: ");
                Toast.makeText(MainActivity.this, "onComplete: ", Toast.LENGTH_SHORT).show();
//                }
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                if (exception instanceof ApiException) {
                    int statusCode = ((ApiException) exception).getStatusCode();
                    text_view.setText("startSmsRetriever, onFailure, status code is: " + statusCode);
                    Log.i("onFailure", "onFailure: ");
                    Toast.makeText(MainActivity.this, "onFailure: ", Toast.LENGTH_SHORT).show();
                }

                exception.printStackTrace();

            }
        });

    }

    @Override
    public void onReadSMS(String otp) {
        Toast.makeText(this, "otp" + "onReadSMS", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTimeOutReadSms(String otp) {
        Toast.makeText(this, "otp" + "onTimeOutReadSms", Toast.LENGTH_SHORT).show();
    }
}
